prefix = thesis
ARGS=-file-line-error -halt-on-error 

.PHONY: all
.PHONY: draft
.PHONY: nodraft
.PHONY: check
.PHONY: check_references
.PHONY: clean


all:
	# draft by default
	pdflatex "\def\isdraft{1} \input{$(prefix).tex}" ${ARGS}
	bibtex $(prefix).aux
	pdflatex "\def\isdraft{1} \input{$(prefix).tex}" ${ARGS}
	pdflatex "\def\isdraft{1} \input{$(prefix).tex}" ${ARGS}

draft:
	# draft by default
	pdflatex "\def\isdraft{1} $(prefix).tex" ${ARGS}
	bibtex $(prefix).aux
	pdflatex "\def\isdraft{1} $(prefix).tex" ${ARGS}
	pdflatex "\def\isdraft{1} $(prefix).tex" ${ARGS}

nodraft:
	pdflatex $(prefix).tex ${ARGS}
	bibtex $(prefix).aux
	pdflatex $(prefix).tex ${ARGS}
	pdflatex $(prefix).tex ${ARGS}

check:
	./checkwriting chapters/*/*.tex

check_references:
	./checkwriting references/refs.bib

clean:
	rm *.out *.log *.aux *.bbl *.blg *.toc
